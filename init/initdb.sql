\set silf_username `echo $SILF_USER`
\set silf_database `echo $SILF_DB`
\set silf_user_pass `echo $SILF_PASS`

CREATE USER :silf_username PASSWORD :'silf_user_pass';

CREATE DATABASE :silf_database OWNER :silf_username;

\connect :silf_database

CREATE EXTENSION btree_gist
